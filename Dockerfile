FROM eclipse-temurin:17 as builder

# Build the app
WORKDIR application
COPY .mvn ./.mvn
COPY pom.xml mvnw ./
RUN ./mvnw dependency:go-offline
COPY src src
RUN ./mvnw -DskipTests package

# Make the executable image
FROM eclipse-temurin:17-jre-alpine

# Get the app
WORKDIR application
COPY --from=builder application/target/*.jar ./app.jar
COPY ods6.txt owl06.txt ./
ENTRYPOINT ["java", "-jar", "app.jar"]
