package com.zenika.academy.scrabbleapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;

import java.util.Random;

@SpringBootApplication
@ConfigurationPropertiesScan
public class ScrabbleApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScrabbleApiApplication.class, args);
    }
    
    @Bean
    public Random random() {
        return new Random();
    }
}
