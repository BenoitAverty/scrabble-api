package com.zenika.academy.scrabbleapi.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.io.File;
import java.util.HashMap;

@ConfigurationProperties("scrabble")
@ConstructorBinding
public record ScrabbleApiProperties(HashMap<String, File> dictionaries) {
}
