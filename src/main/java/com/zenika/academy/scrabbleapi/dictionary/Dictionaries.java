package com.zenika.academy.scrabbleapi.dictionary;

import com.zenika.academy.scrabbleapi.configuration.ScrabbleApiProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

@Component
public class Dictionaries {
    private static final Logger log = LoggerFactory.getLogger(Dictionaries.class);
    
    private final ScrabbleApiProperties scrabbleApiProperties;
    private final Random random;
    private final HashMap<String, Dictionary> dictionaries;

    @Autowired
    public Dictionaries(ScrabbleApiProperties scrabbleApiProperties, Random random) {
        this.scrabbleApiProperties = scrabbleApiProperties;
        this.random = random;
        this.dictionaries = new HashMap<>(scrabbleApiProperties.dictionaries().size());
    }

    /**
     * Initialize dictionaries based on the properties of the app.
     */
    @PostConstruct
    void setupDictionaries() throws IOException {
        log.info("Starting to load dictionaries");
        for (Map.Entry<String, File> entry : this.scrabbleApiProperties.dictionaries().entrySet()) {
            String lang = entry.getKey();
            File file = entry.getValue();
            
            log.debug("Creating dictionary of id {} with file {}", lang, file.getName());
            this.dictionaries.put(
                    lang, 
                    new FileDictionary(
                            new BufferedReader(new FileReader(file)),
                            random
                    )
            );
        }
        log.info("Dictionaries loaded : " + dictionaries.keySet());
    }

    /**
     * Return the dictionary for the given id if it exists.
     */
    public Optional<Dictionary> getDictionary(String dictId) {
        return Optional.ofNullable(dictionaries.get(dictId));
    }
}
