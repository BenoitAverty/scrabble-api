package com.zenika.academy.scrabbleapi.dictionary;

public interface Dictionary {
    /**
     * Return true if the given word exists in the ditionary, false otherwise.
     */
    boolean wordExists(String word);

    /**
     * Return a random word of this dictionary.
     * 
     * @param length the length of the word wanted
     */
    String randomWord(int length);
}
