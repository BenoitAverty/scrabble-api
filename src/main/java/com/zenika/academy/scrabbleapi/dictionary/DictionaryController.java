package com.zenika.academy.scrabbleapi.dictionary;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.zenika.academy.scrabbleapi.dictionary.WordResponseDto.unknownWord;
import static com.zenika.academy.scrabbleapi.dictionary.WordResponseDto.wordExists;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/api/dictionaries")
@RequiredArgsConstructor
public class DictionaryController {
    private final Dictionaries dictionaries;

    @GetMapping("/{dictId}/randomWord")
    public ResponseEntity<WordResponseDto> getRandomWord(@RequestParam("length") int length, @PathVariable String dictId) {

        return dictionaries.getDictionary(dictId)
                .map(d -> WordResponseDto.wordExists(d.randomWord(length)))
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity
                        .status(NOT_FOUND)
                        .body(unknownWord("The dictionary " + dictId + " does not exist"))
                );
    }

    @GetMapping("/{dictId}/words/{word}")
    public ResponseEntity<WordResponseDto> getWord(@PathVariable("word") String word, @PathVariable String dictId) {

        final Optional<Dictionary> dictionary = dictionaries.getDictionary(dictId);
        if (dictionary.isEmpty()) {
            return ResponseEntity
                    .status(NOT_FOUND)
                    .body(unknownWord("The dictionary" + dictId + " does not exist"));
        }
        return dictionary.get().wordExists(word) ?
                ResponseEntity.ok(wordExists(word)) :
                ResponseEntity
                        .status(NOT_FOUND)
                        .body(unknownWord("The word " + word + " does not exist in the dictionary " + dictId));
    }
}
