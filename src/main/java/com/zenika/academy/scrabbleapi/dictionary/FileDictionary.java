package com.zenika.academy.scrabbleapi.dictionary;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Random;

public class FileDictionary implements Dictionary {
    private final HashMap<Integer, ArrayList<String>> wordsByLength = new HashMap<>();
    private final HashSet<String> allWords = new HashSet<>();
    private final Random random;

    public FileDictionary(BufferedReader reader, Random random) throws IOException {
        this.random = random;
        String word = reader.readLine();
        while (word != null) {
            allWords.add(word);
            
            int wordLength = word.length();
            
            if(wordsByLength.containsKey(wordLength)) {
                wordsByLength.get(wordLength).add(word);
            }
            else {
                ArrayList<String> newList = new ArrayList<>();
                newList.add(word);
                wordsByLength.put(wordLength, newList);
            }
            
            word = reader.readLine();
        }
    }

    @Override
    public boolean wordExists(String word) {
        return allWords.contains(word.toUpperCase(Locale.ROOT));
    }

    @Override
    public String randomWord(int length) {
        final ArrayList<String> wordsOfGivenLength = wordsByLength.get(length);
        return wordsOfGivenLength.get(random.nextInt(wordsOfGivenLength.size())).toLowerCase(Locale.ROOT);
    }
}
