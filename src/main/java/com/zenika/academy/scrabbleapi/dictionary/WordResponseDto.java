package com.zenika.academy.scrabbleapi.dictionary;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class WordResponseDto {
    private final boolean wordExists;
    
    public static WordResponseDto wordExists(String word) {
        return new WordExistsResponseDto(word);
    }
    
    public static WordResponseDto unknownWord(String message) {
        return new UnknownWordResponseDto(message);
    }

    @Getter
    public static class WordExistsResponseDto extends WordResponseDto {
        private final String word;
    
        private WordExistsResponseDto(String word) {
            super(true);
            this.word = word;
        }
    }

    @Getter
    public static class UnknownWordResponseDto extends WordResponseDto {
        private final String message;
    
        private UnknownWordResponseDto(String message) {
            super(false);
            this.message = message;
        }
    }
}
